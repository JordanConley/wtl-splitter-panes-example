#include <Windows.h>

#pragma warning(push)
#pragma warning(disable: 4302 4838)
#include <atlbase.h>
#include <atlapp.h>
#include <atlctrls.h>
#include <atlctrlx.h>
#include <atlframe.h>
#include <atlsplit.h>
#pragma warning(pop)

#include <string>

const std::wstring lines[5] = {
	L"Graph",
	L"Virtual Keyboard",
	L"Midi file",
	L"Midi device",
	L"Metronome"
};

template<const int n>
class Child : public CFrameWindowImpl<Child<n>> {
public:
	BEGIN_MSG_MAP(MainWindow)
		MESSAGE_HANDLER(WM_PAINT, HandlePaint)
	END_MSG_MAP()

	LRESULT HandlePaint(UINT, WPARAM, LPARAM, BOOL&) {
		PAINTSTRUCT ps;
		CDC dc = BeginPaint(&ps);

		std::wstring _text = lines[n];

		dc.TextOut(0, 0, _text.c_str(), _text.length());

		EndPaint(&ps);
		return 0;
	}
};

class MainWindow : public CFrameWindowImpl<MainWindow> {
public:
	BEGIN_MSG_MAP(MainWindow)
		MESSAGE_HANDLER(WM_CREATE, HandleCreate)
		MESSAGE_HANDLER(WM_DESTROY, HandleDestroy)
		MESSAGE_HANDLER(WM_SIZE, HandleResize)
	END_MSG_MAP()
	
	CHorSplitterWindow topbottom;
	CHorSplitterWindow midimetr;
	CHorSplitterWindow midiSplit;
	CSplitterWindow vkbdSplit;

	Child<0> graph;
	Child<1> vkbd;
	Child<2> mfile;
	Child<3> mdev;
	Child<4> metr;

	LRESULT HandleCreate(UINT, WPARAM, LPARAM, BOOL&) {
		RECT rc;
		GetClientRect(&rc);
		topbottom.Create(m_hWnd, rcDefault);
		topbottom.SetSplitterPosPct(50);
		topbottom.SetSplitterDefaultPosPct(50);
		topbottom.UpdateWindow();

		vkbdSplit.Create(topbottom, rcDefault);
		vkbdSplit.SetSplitterPosPct(75);
		vkbdSplit.SetSplitterDefaultPos(75);
		vkbdSplit.UpdateWindow();
		topbottom.SetSplitterPane(SPLIT_PANE_BOTTOM, vkbdSplit);

		midimetr.Create(vkbdSplit, rcDefault);
		midimetr.SetSplitterPosPct(50);
		midimetr.SetSplitterDefaultPosPct(50);
		midimetr.UpdateWindow();
		vkbdSplit.SetSplitterPane(SPLIT_PANE_RIGHT, midimetr);

		midiSplit.Create(midimetr, rcDefault);
		midiSplit.SetSplitterPosPct(50);
		midiSplit.SetSplitterDefaultPosPct(50);
		midiSplit.UpdateWindow();
		midimetr.SetSplitterPane(SPLIT_PANE_TOP, midiSplit);

		const DWORD child_style = WS_CHILD | WS_VISIBLE;
		const DWORD child_ex_style = WS_EX_CLIENTEDGE;

		m_hWndClient = topbottom;

		graph.Create(m_hWnd, rcDefault, L"GRPH", child_style, child_ex_style);
		topbottom.SetSplitterPane(SPLIT_PANE_TOP, graph);

		vkbd.Create(vkbdSplit, rcDefault, L"VKBD", child_style, child_ex_style);
		vkbdSplit.SetSplitterPane(SPLIT_PANE_LEFT, vkbd);

		metr.Create(midimetr, rcDefault, L"METR", child_style, child_ex_style);
		midimetr.SetSplitterPane(SPLIT_PANE_BOTTOM, metr);

		mdev.Create(midiSplit, rcDefault, L"MFIL", child_style, child_ex_style);
		mfile.Create(midiSplit, rcDefault, L"MDEV", child_style, child_ex_style);
		midiSplit.SetSplitterPanes(mfile, mdev);
		
	
		return 0;
	}

	LRESULT HandleDestroy(UINT, WPARAM, LPARAM, BOOL&) {
		PostQuitMessage(0);
		return 0;
	}

	LRESULT HandleResize(UINT, WPARAM, LPARAM lParam, BOOL&) {
		topbottom.ResizeClient(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		return 0;
	}
};

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT) {
	RECT rc{ 50, 50, 640, 480 };

	MainWindow wnd;
	wnd.Create(nullptr, &rc, L"MainWindow", WS_OVERLAPPEDWINDOW | WS_VISIBLE, WS_EX_APPWINDOW);
	
	CMessageLoop loop;
	return loop.Run();
}